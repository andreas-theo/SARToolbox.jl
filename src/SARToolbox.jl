module SARToolbox

import Base: +

using SatelliteToolbox
using LinearAlgebra
using StaticArrays
using Tullio
import ThreadsX
################################################################################
#                             Types and Structures
################################################################################

include("types.jl")

include("geometry/viewing_geometry.jl")
include("spectrum/support.jl")
include("spectrum/shift.jl")
include("orbits/formation.jl")
include("orbits/propagation.jl")
# Write your package code here.

const a_wgs84 = 6378137.0
const f_wgs84 = 1 / 298.257223563
const b_wgs84 = a_wgs84 * (1 - f_wgs84)
const Rₑ = 6.3781E6
# Earth's angular rotation [rad/s] without LOD correction.
const w_e = 7.292_115_146_706_979e-5
const c₀ = 299_792_458
const f₀ = 5.405E9
const μₑ = 3.986004415E14

"""
    unit_v(v::AbstractVector)

Normalise the vector `v`.
"""
unit_v(v::AbstractVector) = v ./ LinearAlgebra.norm(v)

function create_reference_orbit(tle_filename::String)
    tle_ref = SatelliteToolbox.read_tles_from_file(tle_filename) |> first
    # Constants
    revday2radsec = 2π / 86400    # Revolutions per day to radians per second.
    d2r = π / 180      # Degrees to radians.
    j2_constants = SatelliteToolbox.j2c_egm2008
    # Obtain the data from the TLE.
    n_0 = tle_ref.mean_motion * revday2radsec
    e_0 = tle_ref.eccentricity
    i_0 = tle_ref.inclination * d2r
    Ω_0 = tle_ref.raan * d2r
    ω_0 = tle_ref.argument_of_perigee * d2r
    M_0 = tle_ref.mean_anomaly * d2r

    # Auxiliary variables.
    e_0² = e_0^2
    cos_i_0 = cos(i_0)

    # Recover the original mean motion (nll_0) and semi-major axis (all_0) from
    # the input elements and the same algorithm of SGP4.
    aux = 1 / 2 * j2_constants.J2 * (3 * cos_i_0^2 - 1) / (1 - e_0²)^(3 / 2)
    a_1 = (j2_constants.μm / n_0)^(2 / 3)
    δ_1 = 3 / 2 * aux / a_1^2
    a_0 = a_1 * (1 - 1 / 3 * δ_1 - δ_1^2 - 134 / 81 * δ_1^3)
    δ_0 = 3 / 2 * aux / a_0^2
    nll_0 = n_0 / (1 + δ_0)
    a_0 = (j2_constants.μm / nll_0)^(2 / 3) * j2_constants.R0
    f_0 = SatelliteToolbox.mean_to_true_anomaly(e_0, M_0)
    if tle_ref.epoch_year > 75
        epoch =
            datetime2julian(DateTime(1900 + tle_ref.epoch_year, 1, 1, 0, 0, 0)) - 1 +
            tle_ref.epoch_day
    else
        epoch =
            datetime2julian(DateTime(2000 + tle_ref.epoch_year, 1, 1, 0, 0, 0)) - 1 +
            tle_ref.epoch_day
    end
    orb = SatelliteToolbox.KeplerianElements(epoch, a_0, e_0, i_0, Ω_0, ω_0, f_0)
    Propagators.init(Val(:J2), orb; j2c = j2_constants)
end

function from_rel_orbit(
    prop_type,
    ref_orbit::SatelliteToolboxBase.KeplerianElements,
    Δe::Number,
    ΔΩ::Number,
    Δf::Number = 0,
)
    rel_orbit = RelativeOrbit(0, Δe, 0, ΔΩ, 0, round(Δf, digits = 6))
    Propagators.init(prop_type, ref_orbit + rel_orbit)
end

"""
    los_to_surface(r⃗::AbstractVector, b̂::AbstractVector)

Find the point of intersection of the line of sight with Earth's surface.

Starting from the position vector `r⃗`, go in the direction of the unit vector `b̂` pointing in the LoS until an intersection with the WGS84 geoid is found.
"""
function los_to_surface(r⃗::AbstractVector, b̂::AbstractVector)
    af = (b̂[1] / a_wgs84)^2 + (b̂[2] / a_wgs84)^2 + (b̂[3] / b_wgs84)^2
    bf =
        2 * (
            (r⃗[1] * b̂[1] / a_wgs84^2) +
            (r⃗[2] * b̂[2] / a_wgs84^2) +
            (r⃗[3] * b̂[3] / b_wgs84^2)
        )
    cf = (r⃗[1] / a_wgs84)^2 + (r⃗[2] / a_wgs84)^2 + (r⃗[3] / b_wgs84)^2 - 1
    discr = bf^2 - 4 * af * cf
    if discr < 0
        error(
            "The Line-of-Sight vector does not point toward the Earth. At some positions there is no intersection with the ellipsoid. Please check your inputs.",
        )
    else
        root_1 = (-bf - √discr) / (2af)
        root_2 = (-bf + √discr) / (2af)
    end
    r1_is_negative = root_1 < 0
    r2_is_negative = root_2 < 0
    if r1_is_negative && r2_is_negative
        error("There are intersections in the backward direction.")
    end
    if r1_is_negative
        root_1 = Inf
    end
    if r2_is_negative
        root_2 = Inf
    end
    root = minimum((root_1, root_2))
    r⃗ + (b̂ * root)
end

"""
    wave_vector(θᵢ::Number, ψ::Number)

Compute the unit wave vector, also known as the line of sight vector, for look angle `θᵢ` and squint `ψ`.

Angles are in radians. The vector is computed from the satellite to the surface.
"""
wave_vector(θᵢ::Number, ψ::Number) = [-cos(θᵢ) * cos(ψ), sin(ψ), -sin(θᵢ) * cos(ψ)]

function run_parallel_sensitivity(fr)
    orbp = create_reference_orbit("assets/sentinel-1B")
    orbp_h = from_rel_orbit(
        Propagators.mean_elements(orbp),
        0,
        0,
        -SatelliteToolbox.mean_to_true_anomaly(
            Propagators.mean_elements(orbp).e,
            300e3 / Propagators.mean_elements(orbp).a,
        ),
    )
    harmony_formation = RelativeOrbitalElements(
        0,
        0,
        0,
        100 / Propagators.mean_elements(orbp_h).a,
        0,
        800 / Propagators.mean_elements(orbp_h).a,
    )
    inc_range = deg2rad.(30:0.2:50)
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    asc_t = ascend_descend(orbp, 0.001)
    time_vectors = (t:1e-4:t+1 for t = asc_t:30:(asc_t+T/2))
    ThreadsX.mapi(
        height_sensitivity,
        Iterators.repeated((orbp, orbp_h)),
        Iterators.repeated(harmony_formation),
        Iterators.repeated(inc_range),
        Iterators.repeated(fr |> collect),
        time_vectors,
    )
end

function propagate_companion(o_h1, r_h1, v_h1, formation)
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr = relative_orb_to_Δr(formation, o_h1[i].a, u)
        r_h2[i] = chief_to_deputy_r(r_h1[i], v_h1[i], Δr)
    end
    r_h2
end

function satellites_to_ECEF(o_s1, r_s1, v_s1, r_h1, r_h2)
    # Convert to ECEF to model rotation of Earth
    epoch_jd = map(x -> x.t, o_s1)
    r_s1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_s1)
    mid_index = floor(Int, length(r_s1_e) / 2)
    v_s1_e_mid =
        r_eci_to_ecef(TEME(), PEF(), epoch_jd[mid_index]) * v_s1[mid_index] -
        [0; 0; w_e] × r_s1_e[mid_index]
    r_h1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_h1)
    r_h2_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_h2)
    r_s1_e, v_s1_e_mid, r_h1_e, r_h2_e
end

function ground_point_from_position(r_c, Q_EC_to_LVLH_mid, inc_range)
    b̂ = wave_vector.(inc_range, 0)
    mid_index = floor(Int, length(r_c) / 2)

    b̂_ECEF = (Q_EC_to_LVLH_mid,) .* b̂
    r_c_mid = @views r_c[mid_index]
    r_p = los_to_surface.((r_c_mid,), b̂_ECEF)
    r_p
end

function temporal_diff(
    propagators,
    formation,
    inc_range,
    fr,
    t::AbstractVector,
    window_length::Int,
)
    orbp_s1, orbp_h1 = deepcopy(propagators[1]), deepcopy(propagators[2])
    o_s1, r_s1, v_s1 = propagate_with_mean_elements!(orbp_s1, t)
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t)
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr = relative_orb_to_Δr(formation, o_h1[i].a, u)
        r_h2[i] = chief_to_deputy_r(r_h1[i], v_h1[i], Δr)
    end

    # Convert to ECEF to model rotation of Earth
    epoch_jd = map(x -> x.t, o_s1)
    r_s1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_s1)
    mid_index = floor(Int, length(r_s1_e) / 2)
    sv = SatelliteToolbox.OrbitStateVector(
        epoch_jd[mid_index],
        r_s1[mid_index],
        v_s1[mid_index],
    )
    sv = SatelliteToolbox.sv_eci_to_ecef(
        sv,
        SatelliteToolbox.TEME(),
        SatelliteToolbox.PEF(),
        epoch_jd[mid_index],
    )
    v_s1_e_mid = sv.v
    r_s1_e_mid = r_s1_e[mid_index]
    r_h1_e_mid = r_eci_to_ecef(TEME(), PEF(), epoch_jd[mid_index]) * r_h1[mid_index]
    r_h2_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_h2)

    @tullio r_p[k] := find_intersection_with_surface(inc_range[k], r_s1_e_mid, v_s1_e_mid)
    central_fr_index = ceil(Int, size(fr, 1) / 2)
    central_fr = fr[central_fr_index]
    @tullio sh1[k] := SARToolbox.compute_support_new(
        r_s1_e_mid,
        r_s1_e_mid,
        r_h1_e_mid,
        r_p[k],
        central_fr,
    )
    @tullio sh2[i, j, k] :=
        SARToolbox.compute_support_new(r_s1_e_mid, r_s1_e[i], r_h2_e[i], r_p[k], fr[j])
    indices = spectral_shift_loop(sh1, sh2, 1, window_length)
    dt = t[2] - t[1]
    t_lag = [(window_length + 1 - i[1]) * dt for i in indices]
    f_shift = [fr[i[2]] for i in indices]
    (t_lag, f_shift)
end

function convert_to_ecef(t, r, v)
    sv = SatelliteToolbox.OrbitStateVector(t, r, v)
    sv = SatelliteToolbox.sv_eci_to_ecef(
        sv,
        SatelliteToolbox.TEME(),
        SatelliteToolbox.PEF(),
        t,
    )
    return sv
end

function sensitivity_analytical(propagators, formation, look_angle, t)
    Δt, Δk = temporal_diff_analytical(propagators, formation, look_angle, t)
    orbp_s1, orbp_h1 = deepcopy(propagators[1]), deepcopy(propagators[2])
    t_shifted = t + Δt
    r_s1, v_s1 = Propagators.propagate!(orbp_s1, t)
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t)
    epoch = o_h1.t
    svs = map(((r, v),) -> convert_to_ecef(epoch, r, v), ((r_s1, v_s1), (r_h1, v_h1)))
    r_s1_e, r_h1_e = getfield.(svs, :r)
    v_s1_e, v_h1_e = getfield.(svs, :v)
    r_p = find_intersection_with_surface(look_angle, r_s1_e, v_s1_e)
    # We do not pass an r_ref to compute the spectral support in the ECEF frame. ζ̂ and z⃗ are defined in ECEF so Δs should be in ECEF too, otherwise the dot product with ζ̂ doesn't make sense.
    sh1 = SARToolbox.compute_support_new(r_s1_e, r_h1_e, r_p, 0)
    # project the spectral difference along the direction to which the interferometric phase is sensitive
    r_s1_p = -r_s1_e + r_p
    r_h1_p = -r_h1_e + r_p
    r_bistatic = unit_v(r_s1_p) + unit_v(r_h1_p)
    ζ⃗ = r_bistatic × (-v_s1_e / norm(r_s1_p) - v_h1_e / norm(r_h1_p))
    ζ̂ = unit_v(ζ⃗)
    _, _, z⃗ = find_tangent_normal_basis(r_p, unit_v(r_h1_p))
    # Now we recalculate the positions at the temporal lag
    orbp_s1, orbp_h1 = deepcopy(propagators[1]), deepcopy(propagators[2])
    r_s1, _ = Propagators.propagate!(orbp_s1, t_shifted)
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t_shifted)
    u = o_h1.ω + SatelliteToolbox.true_to_mean_anomaly(o_h1.e, o_h1.f)
    Δr = relative_orb_to_Δr(formation, o_h1.a, u)
    r_h2 = chief_to_deputy_r(r_h1, v_h1, Δr)
    epoch = o_h1.t
    r_s1_e =
        SatelliteToolbox.r_eci_to_ecef(
            SatelliteToolbox.TEME(),
            SatelliteToolbox.PEF(),
            epoch,
        ) * r_s1
    r_h2_e =
        SatelliteToolbox.r_eci_to_ecef(
            SatelliteToolbox.TEME(),
            SatelliteToolbox.PEF(),
            epoch,
        ) * r_h2
    f_shift = Δk * c₀ / (2π)
    sh2 = compute_support_new(r_s1_e, r_h2_e, r_p, f_shift)
    Δs = sh1 - sh2
    sensitivity = Δs ⋅ ζ̂ / (ζ̂ ⋅ z⃗)
    sensitivity
end

function sensitivity_analytical_mono(propagators, formation, look_angle, t)
    Δt, Δk = temporal_diff_analytical_mono(propagators, formation, look_angle, t)
    orbp_h1 = deepcopy(propagators[2])
    t_shifted = t + Δt
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t)
    epoch = o_h1.t
    sv = convert_to_ecef(epoch, r_h1, v_h1)
    r_h1_e = getfield(sv, :r)
    v_h1_e = getfield(sv, :v)
    r_p = find_intersection_with_surface(look_angle, r_h1_e, v_h1_e)
    # We do not pass an r_ref to compute the spectral support in the ECEF frame. ζ̂ and z⃗ are defined in ECEF so Δs should be in ECEF too, otherwise the dot product with ζ̂ doesn't make sense.
    sh1 = SARToolbox.compute_support_new(r_h1_e, r_h1_e, r_p, 0)
    # project the spectral difference along the direction to which the interferometric phase is sensitive
    r_h1_p = -r_h1_e + r_p
    r_bistatic = 2 * unit_v(r_h1_p)
    ζ⃗ = r_bistatic × (-v_h1_e / norm(r_h1_p) - v_h1_e / norm(r_h1_p))
    ζ̂ = unit_v(ζ⃗)
    _, _, z⃗ = find_tangent_normal_basis(r_p, unit_v(r_h1_p))
    # Now we recalculate the positions at the temporal lag
    orbp_h1 = deepcopy(propagators[2])
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t_shifted)
    u = o_h1.ω + SatelliteToolbox.true_to_mean_anomaly(o_h1.e, o_h1.f)
    Δr = relative_orb_to_Δr(formation, o_h1.a, u)
    r_h2 = chief_to_deputy_r(r_h1, v_h1, Δr)
    epoch = o_h1.t
    r_h2_e =
        SatelliteToolbox.r_eci_to_ecef(
            SatelliteToolbox.TEME(),
            SatelliteToolbox.PEF(),
            epoch,
        ) * r_h2
    f_shift = Δk * c₀ / (2π)
    sh2 = compute_support_new(r_h2_e, r_h2_e, r_p, f_shift)
    Δs = sh1 - sh2
    sensitivity = Δs ⋅ ζ̂ / (ζ̂ ⋅ z⃗)
    sensitivity
end

function perpendicular_baseline(propagators, formation, look_angle, t)
    Δt, _ = temporal_diff_analytical(propagators, formation, look_angle, t)
    orbp_s1, orbp_h1 = deepcopy(propagators[1]), deepcopy(propagators[2])
    t_shifted = t + Δt
    r_s1, v_s1 = Propagators.propagate!(orbp_s1, t)
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t)
    epoch = o_h1.t
    svs = map(((r, v),) -> convert_to_ecef(epoch, r, v), ((r_s1, v_s1), (r_h1, v_h1)))
    r_s1_e, r_h1_e = getfield.(svs, :r)
    v_s1_e, v_h1_e = getfield.(svs, :v)
    r_p = find_intersection_with_surface(look_angle, r_s1_e, v_s1_e)
    # We do not pass an r_ref to compute the spectral support in the ECEF frame. ζ̂ and z⃗ are defined in ECEF so Δs should be in ECEF too, otherwise the dot product with ζ̂ doesn't make sense.
    # project the spectral difference along the direction to which the interferometric phase is sensitive
    r_s1_p = -r_s1_e + r_p
    r_h1_p = -r_h1_e + r_p
    r_bi_unormalised = r_s1_p + r_h1_p
    r_bistatic = unit_v(r_s1_p) + unit_v(r_h1_p)
    ζ⃗ = r_bistatic × (-v_s1_e / norm(r_s1_p) - v_h1_e / norm(r_h1_p))
    ζ̂ = unit_v(ζ⃗)
    # Now we recalculate the positions at the temporal lag
    orbp_s1, orbp_h1 = deepcopy(propagators[1]), deepcopy(propagators[2])
    r_s1, _ = Propagators.propagate!(orbp_s1, t_shifted)
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t_shifted)
    u = o_h1.ω + SatelliteToolbox.true_to_mean_anomaly(o_h1.e, o_h1.f)
    Δr = relative_orb_to_Δr(formation, o_h1.a, u)
    r_h2 = chief_to_deputy_r(r_h1, v_h1, Δr)
    epoch = o_h1.t
    r_h2_e =
        SatelliteToolbox.r_eci_to_ecef(
            SatelliteToolbox.TEME(),
            SatelliteToolbox.PEF(),
            epoch,
        ) * r_h2
    Δr_ecef = r_h2_e - r_h1_e
    B_perp = Δr_ecef ⋅ ζ̂
    B_perp
end

function temporal_diff_analytical(propagators, formation, look_angle, t)
    orbp_s1, orbp_h1 = deepcopy(propagators[1]), deepcopy(propagators[2])
    o_s1, r_s1, v_s1 = propagate_with_mean_elements!(orbp_s1, t)
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t)
    u = o_h1.ω + SatelliteToolbox.true_to_mean_anomaly(o_h1.e, o_h1.f)
    Δr = relative_orb_to_Δr(formation, o_h1.a, u)
    r_h2 = chief_to_deputy_r(r_h1, v_h1, Δr)
    Δv = relative_orb_to_Δv(formation, o_h1.a, u)
    v_h2 = chief_to_deputy_v(r_h1, v_h1, Δr, Δv, o_h1.a)
    # Convert to ECEF to model rotation of Earth
    epoch_jd = o_s1.t
    sv = convert_to_ecef(epoch_jd, r_s1, v_s1)
    v_s1_e = sv.v
    r_s1_e = sv.r
    sv = convert_to_ecef(epoch_jd, r_h1, v_h1)
    v_h1_e = sv.v
    r_h1_e = sv.r
    sv = convert_to_ecef(epoch_jd, r_h2, v_h2)
    r_h2_e, v_h2_e = sv.r, sv.v
    r_p = find_intersection_with_surface(look_angle, r_s1_e, v_s1_e)
    b̂_ref = unit_v(-r_s1_e + r_p)
    # Convert the position and velocity vectors to surface tangent-normal basis
    Q_EC_to_LTLN = EC_to_tangent_normal(r_p, b̂_ref)
    r_s1_e, r_h1_e, r_h2_e, v_s1_e, v_h1_e, v_h2_e, r_p =
        map(r -> Q_EC_to_LTLN * r, [r_s1_e, r_h1_e, r_h2_e, v_s1_e, v_h1_e, v_h2_e, r_p])
    # Here we were computing the temporal and spectral shift but now that functionality has been moved to the temporal_and_spectral_shift function
    temporal_and_spectral_shift(
        r_s1_e,
        r_s1_e,
        r_h1_e,
        r_h2_e,
        r_p,
        v_s1_e,
        v_s1_e,
        v_h1_e,
        v_h2_e,
    )
end

function temporal_diff_analytical_mono(propagators, formation, look_angle, t)
    orbp_h1 = deepcopy(propagators[2])
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t)
    u = o_h1.ω + SatelliteToolbox.true_to_mean_anomaly(o_h1.e, o_h1.f)
    Δr = relative_orb_to_Δr(formation, o_h1.a, u)
    r_h2 = chief_to_deputy_r(r_h1, v_h1, Δr)
    Δv = relative_orb_to_Δv(formation, o_h1.a, u)
    v_h2 = chief_to_deputy_v(r_h1, v_h1, Δr, Δv, o_h1.a)
    # Convert to ECEF to model rotation of Earth
    epoch_jd = o_h1.t
    sv = convert_to_ecef(epoch_jd, r_h1, v_h1)
    v_h1_e = sv.v
    r_h1_e = sv.r
    sv = convert_to_ecef(epoch_jd, r_h2, v_h2)
    r_h2_e, v_h2_e = sv.r, sv.v
    r_p = find_intersection_with_surface(look_angle, r_h1_e, v_h1_e)
    b̂_ref = unit_v(-r_h1_e + r_p)
    # Convert the position and velocity vectors to surface tangent-normal basis
    Q_EC_to_LTLN = EC_to_tangent_normal(r_p, b̂_ref)
    r_h1_e, r_h2_e, v_h1_e, v_h2_e, r_p =
        map(r -> Q_EC_to_LTLN * r, [r_h1_e, r_h2_e, v_h1_e, v_h2_e, r_p])
    temporal_and_spectral_shift(
        r_h1_e,
        r_h2_e,
        r_h1_e,
        r_h2_e,
        r_p,
        v_h1_e,
        v_h2_e,
        v_h1_e,
        v_h2_e,
    )
end

function temporal_and_spectral_shift(r_t1, r_t2, r_r1, r_r2, r_p, v_t1, v_t2, v_r1, v_r2)
    l̂ₜ₁ = unit_v(-r_t1 + r_p)
    l̂ᵣ₁ = unit_v(-r_r1 + r_p)
    l̂ₜ₂ = unit_v(-r_t2 + r_p)
    l̂ᵣ₂ = unit_v(-r_r2 + r_p)
    Δl̂ₜ = l̂ₜ₂ - l̂ₜ₁
    Δl̂ᵣ = l̂ᵣ₂ - l̂ᵣ₁
    l̇ₜ₂ = (-v_t2 + l̂ₜ₂ ⋅ v_t2 * l̂ₜ₂) / norm(-r_t2 + r_p)
    l̇ᵣ₂ = (-v_r2 + l̂ᵣ₂ ⋅ v_r2 * l̂ᵣ₂) / norm(-r_r2 + r_p)
    l̇ₜ = (-v_t1 + l̂ₜ₁ ⋅ v_t1 * l̂ₜ₁) / norm(-r_t1 + r_p)
    l̇ᵣ = (-v_r1 + l̂ᵣ₁ ⋅ v_r1 * l̂ᵣ₁) / norm(-r_r1 + r_p)
    Δl̇ = l̇ₜ₂ - l̇ₜ + l̇ᵣ₂ - l̇ᵣ
    k₀ = 2π * f₀ / c₀
    l̇_total = l̇ₜ + l̇ᵣ + Δl̇
    Δl̂_total = Δl̂ₜ + Δl̂ᵣ
    l̂_total = l̂ₜ₁ + l̂ᵣ₁ + Δl̂_total
    A = [
        k₀*l̇_total[2] l̂_total[2]
        k₀*l̇_total[3] l̂_total[3]
    ]
    b = -k₀ * Δl̂_total[2:3]
    Δt_Δk = A \ b
    (Δt_Δk[1], Δt_Δk[2])
end

function temporal_diff_flat_analytical(r_h1, r_h2, v_h1, inc_angle)
    r_p = @SVector [0.0, r_h1[2], 693E3 * tan(inc_angle)]
    b̂_h1 = unit_v(-r_h1 + r_p)
    b̂_h2 = unit_v(-r_h2 + r_p)
    b̂̇_h1 = (-v_h1 + b̂_h1 ⋅ v_h1 * b̂_h1) / norm(-r_h1 + r_p)
    Δb̂ = b̂_h2 - b̂_h1
    k₀ = 2π * f₀ / c₀
    b̂̇_bistatic = 2 * b̂̇_h1
    # b̂̇_bistatic = b̂̇_h1           # for monostatic
    b̂_bistatic = b̂_h1 + b̂_h2
    # b̂_bistatic = b̂_h2           # for monostatic
    A = [
        k₀*b̂̇_bistatic[3] b̂_bistatic[3]
        k₀*b̂̇_bistatic[2] b̂_bistatic[2]
    ]
    k_zero = -k₀ * Δb̂[3:-1:2]
    Δt_Δk = A \ k_zero
    (Δt_Δk[1], Δt_Δk[2])
end

function sensitivity_flat_analytical(r_h1, r_h2, v_h1, look_angle)
    _, Δk = temporal_diff_flat_analytical(r_h1, r_h2, v_h1, look_angle)
    r_p = @SVector [0.0, r_h1[2], 693E3 * tan(look_angle)]
    sh1 = SARToolbox.compute_support_flat(r_h1, r_h1, r_p, 0)
    f_shift = Δk * c₀ / (2π)
    sh2 = SARToolbox.compute_support_flat(r_h2, r_h2, r_p, f_shift)
    Δs = sh1 - sh2
    # project the spectral difference along the direction to which the interferometric phase is sensitive
    r_h1_p = -r_h1 + r_p
    r_bistatic = 2 * unit_v(r_h1_p)
    ζ⃗ = r_bistatic × (-2 * v_h1 / norm(r_h1_p))
    ζ̂ = unit_v(ζ⃗)
    z⃗ = [1, 0, 0]
    l̂ = unit_v(r_h1_p)
    sensitivity = Δs ⋅ ζ̂ / (ζ̂ ⋅ z⃗)
    Δr = r_h2 - r_h1
    Δr_dot_l = Δr ⋅ l̂
    B_perp = sign(Δr_dot_l) * norm(Δr - Δr_dot_l * l̂)
    λ₀ = c₀ / f₀
    Rₛ = norm(r_h1_p)
    h_amb_cl = λ₀ * Rₛ * sin(look_angle) / (2 * B_perp)
    sensitivity_cl = 2π / h_amb_cl
    (sensitivity, sensitivity_cl)
end

function temporal_diff_flat(r_h1, r_h2, inc_range, fr, dt, window_length::Int)
    r_p = zeros(SVector{3,Float64}, length(inc_range))
    mid_index = floor(Int, lastindex(r_h1) / 2)
    for i in eachindex(r_p)
        r_p[i] = @SVector [0.0, r_h1[mid_index][2], 693E3 * tan(inc_range[i])]
    end
    r_h1_mid = getindex(r_h1, mid_index)
    central_fr_index = ceil(Int, size(fr, 1) / 2)
    central_fr = fr[central_fr_index]
    Tullio.@tullio sh1[k] :=
        SARToolbox.compute_support_flat(r_h1_mid, r_h1_mid, r_p[k], central_fr)
    Tullio.@tullio sh2[i, j, k] :=
        SARToolbox.compute_support_flat(r_h1[i], r_h2[i], r_p[k], fr[j])
    indices = spectral_shift_loop(sh1, sh2, 1, window_length)
    [(window_length + 1 - i[1]) * dt for i in indices]
end

function temporal_diff_flat_separate_illuminator(
    r_s1,
    r_h1,
    r_h2,
    inc_range,
    fr,
    dt,
    window_length::Int,
)
    r_p = zeros(SVector{3,Float64}, length(inc_range))
    mid_index = floor(Int, lastindex(r_h1) / 2)
    for i in eachindex(r_p)
        r_p[i] = @SVector [0.0, r_s1[mid_index][2], 693E3 * tan(inc_range[i])]
    end
    r_s1_mid, r_h1_mid = getindex.((r_s1, r_h1), mid_index)
    central_fr_index = ceil(Int, size(fr, 1) / 2)
    central_fr = fr[central_fr_index]
    Tullio.@tullio sh1[k] :=
        SARToolbox.compute_support_flat(r_s1_mid, r_h1_mid, r_p[k], central_fr)
    Tullio.@tullio sh2[i, j, k] :=
        SARToolbox.compute_support_flat(r_s1[i], r_h2[i], r_p[k], fr[j])
    indices = spectral_shift_loop(sh1, sh2, 1, window_length)
    [(window_length + 1 - i[1]) * dt for i in indices]
end

function height_sensitivity(propagators, formation, inc_range, fr, t::AbstractVector)
    orbp_s1, orbp_h1 = deepcopy(propagators[1]), deepcopy(propagators[2])
    o_s1, r_s1, v_s1 = propagate_with_mean_elements!(orbp_s1, t)
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t)
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr = relative_orb_to_Δr(formation, o_h1[i].a, u)
        r_h2[i] = chief_to_deputy_r(r_h1[i], v_h1[i], Δr)
    end

    # Convert to ECEF to model rotation of Earth
    epoch_jd = map(x -> x.t, o_s1)
    r_s1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_s1)
    v_s1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, v_s1)
    r_h1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_h1)
    r_h2_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_h2)
    Qs = ECI_to_LVLH_matrices(r_s1_e, v_s1_e)
    sh1, sh2 = (
        compute_support(r_s1_e, r_h1_e, Qs, inc_range, fr),
        compute_support(r_s1_e, r_h2_e, Qs, inc_range, fr),
    )
    indices = spectral_shift_loop(sh1, sh2, 1, 250)

    b̂ = wave_vector.(inc_range, 0)
    mid_index = floor(Int, length(r_s1_e) / 2)
    Q_EC_to_LVLH_mid = @views Qs[mid_index]
    b̂_ECEF = (transpose(Q_EC_to_LVLH_mid),) .* b̂
    r_s1_e_mid = @views r_s1_e[mid_index]
    r_p = los_to_surface.((r_s1_e_mid,), b̂_ECEF)
    map(
        (i, r_pixel) -> sensitivity(
            sh1[mid_index, i[2], i[3]],
            sh2[mid_index-i[1]-250, i[2], i[3]],
            r_pixel,
        ),
        indices,
        r_p,
    )
end

function ascend_descend(orbp::OrbitPropagator{T} where {T}, dt)
    T = SatelliteToolbox.orbital_period(Propagators.mean_elements(orbp))
    time_range = 0:dt:T
    # Copy orbit structure so that it is not modified by `propagate`.
    orbp_c = deepcopy(orbp)
    _, _, v = propagate_with_mean_elements!(orbp_c, time_range)
    sign_diff = diff(sign.([v_i[end] for v_i in v]))
    ascending_index = findfirst(x -> x > 0, sign_diff) + 1
    time_vector = time_range |> collect
    time_vector[ascending_index]
end

"""
    ground_track(epoch_jd, r_i, eop_data::Union{Nothing, EOPData_IAU1980, EOPData_IAU2000A} = nothing; ECI = TEME(), ECEF = PEF(), span = 1.0)

Compute the ground trace of the object with orbit defined by `orbp`.

By default, it considers that the orbit elements on the propagator are
represented in the True Equator, Mean Equinox (TEME) reference frame and the
ground trace will be computed in the Pseudo-Earth Fixed (PEF) reference frame.
Hence, no EOP data is needed. However, this can be changed by the keywords
presented as follows.

# Keywords

* `eop_data`: EOP data that will be used to convert the ECI reference frame to
              the ECEF reference frame. If `nothing`, then it will not be used
              (see `r_eci_to_ecef`). (**Default** = `nothing`)
* `ECI`: ECI frame in which the orbit elements in `orbp` are represented.
         (**Default** = `TEME()`)
* `ECEF`: ECEF frame that will be used to compute the ground trace.
          (**Default** = `PEF()`)

# Returns

A tuple with the pairs `(latitude,longitude)` of the ground track.

"""
function ground_track(
    epoch_jd,
    r_i;
    eop_data::Union{Nothing,EopIau1980,EopIau2000A} = nothing,
    ECI = TEME(),
    ECEF = PEF(),
)

    # Convert from the ECI to the ECEF frame.
    if eop_data == nothing
        r_e = r_eci_to_ecef(ECI, ECEF, epoch_jd) * r_i
    else
        r_e = r_eci_to_ecef(ECI, ECEF, epoch_jd, eop_data) * r_i
    end

    # Convert to Geodetic coordinates.
    geod = ECEFtoGeodetic(r_e)
    (geod[1], geod[2])
end

function find_intersection_with_surface(look_angle, r, v)
    b̂ = SARToolbox.wave_vector(look_angle, 0)
    Q = SARToolbox.ECI_to_LVLH(r, v)
    Q = transpose(Q)
    b̂_EC = Q * b̂
    SARToolbox.los_to_surface(r, b̂_EC)
end

function compute_support(r_c, r_d, Q_EC_to_LVLH_mid::AbstractMatrix, inc_range, fr)
    b̂ = wave_vector.(inc_range, 0)
    mid_index = floor(Int, length(r_c) / 2)

    b̂_ECEF = (Q_EC_to_LVLH_mid,) .* b̂
    r_c_mid = @views r_c[mid_index]
    r_p = los_to_surface.((r_c_mid,), b̂_ECEF)

    Q_EC_to_LTLN = EC_to_tangent_normal.(r_p, b̂_ECEF)
    @tullio r_bistatic[i, j] := unit_v(-r_c[i] + r_p[j]) + unit_v(-r_d[i] + r_p[j])
    spectral_support =
        similar(r_bistatic, (size(r_bistatic)[1], length(fr), size(r_bistatic)[2]))
    for (i, c) in enumerate(eachcol(r_bistatic))
        spectral_support[:, :, i] .=
            SARToolbox.wavenumber_spectrum.(permutedims(fr), (Q_EC_to_LTLN[i],) .* c)
    end
    spectral_support
end

function compute_support(r_ref, r_c, r_d, r_p, fr)
    mid_index = floor(Int, length(r_c) / 2)
    b̂ = unit_v.((-r_ref[mid_index],) .+ r_p)
    Q_EC_to_LTLN = EC_to_tangent_normal.(r_p, b̂)
    @tullio r_bistatic[i, j] := unit_v(-r_c[i] + r_p[j]) + unit_v(-r_d[i] + r_p[j])
    @tullio spectral_support[i, j, k] :=
        wavenumber_spectrum(fr[j], Q_EC_to_LTLN[k] * r_bistatic[i, k])
    spectral_support
end

function compute_support_new(r_ref, r_c, r_d, r_p, fr)
    b̂ = unit_v(-r_ref + r_p)
    Q_EC_to_LTLN = EC_to_tangent_normal(r_p, b̂)
    r_bistatic = unit_v(-r_c + r_p) + unit_v(-r_d + r_p)
    spectral_support = wavenumber_spectrum(fr, Q_EC_to_LTLN * r_bistatic)
    spectral_support
end

function compute_support_new(r_c, r_d, r_p, fr)
    r_bistatic = unit_v(-r_c + r_p) + unit_v(-r_d + r_p)
    spectral_support = wavenumber_spectrum(fr, r_bistatic)
    spectral_support
end

function compute_support_flat(r_c, r_d, r_p, fr)
    r_bistatic = unit_v(-r_c + r_p) + unit_v(-r_d + r_p)
    spectral_support = wavenumber_spectrum(fr, r_bistatic)
    spectral_support
end

function compute_support_ECEF(
    orbp_s1::OrbitPropagator,
    orbp_h1::OrbitPropagator,
    harmony_formation::RelativeOrbitalElements,
    inc_range::AbstractVector,
    t::AbstractVector,
)
    o_s1, r_s1, v_s1 = propagate_with_mean_elements!(orbp_s1, t)
    o_h1, r_h1, v_h1 = propagate_with_mean_elements!(orbp_h1, t)
    r_h2 = similar(r_h1)
    for i in eachindex(o_h1)
        u = o_h1[i].ω + SatelliteToolbox.true_to_mean_anomaly(o_h1[i].e, o_h1[i].f)
        Δr = relative_orb_to_Δr(harmony_formation, o_h1[i].a, u)
        r_h2[i] = chief_to_deputy_r(r_h1[i], v_h1[i], Δr)
    end

    b̂ = wave_vector.(inc_range, 0)
    mid_index = floor(Int, length(o_s1) / 2)

    # Convert to ECEF to model rotation of Earth
    epoch_jd = map(x -> x.t, o_s1)
    r_s1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_s1)
    v_s1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, v_s1)
    r_s1_e_mid = r_s1_e[mid_index]
    v_s1_e_mid = v_s1_e[mid_index]
    r_h1_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_h1)
    r_h2_e = map((t, x) -> r_eci_to_ecef(TEME(), PEF(), t) * x, epoch_jd, r_h2)

    (i_mid, j_mid, k_mid) = find_LVLH_basis(r_s1_e_mid, v_s1_e_mid)
    Q_ECEF_to_LVLH_mid = [transpose(i_mid); transpose(j_mid); transpose(k_mid)]
    b̂_ECEF = (transpose(Q_ECEF_to_LVLH_mid),) .* b̂
    r_p = los_to_surface.((r_s1_e_mid,), b̂_ECEF)
    Qs = ECI_to_LVLH_matrices(r_s1_e, v_s1_e)
    fr = range(-25e6, 25e6, length = 31) |> collect
    r_bistatic =
        [unit_v(r_i - p) for r_i ∈ r_s1_e, p ∈ r_p] .+ [unit_v(r_i - p) for r_i ∈ r_h1_e, p ∈ r_p]
    spectrum_h = similar(r_bistatic, (size(r_bistatic)[1], length(fr), size(r_bistatic)[2]))
    for (i, c) in enumerate(eachcol(r_bistatic))
        spectrum_h[:, :, i] .= SARToolbox.wavenumber_spectrum.(permutedims(fr), Qs .* c)
    end
    r_bistatic .=
        [unit_v(r_i - p) for r_i ∈ r_s1_e, p ∈ r_p] .+ [unit_v(r_i - p) for r_i ∈ r_h2_e, p ∈ r_p]
    spectrum_2 = similar(r_bistatic, (size(r_bistatic)[1], length(fr), size(r_bistatic)[2]))
    for (i, c) in enumerate(eachcol(r_bistatic))
        spectrum_2[:, :, i] .= SARToolbox.wavenumber_spectrum.(permutedims(fr), Qs .* c)
    end
    (spectrum_h, spectrum_2)
end

end
