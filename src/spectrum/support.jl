export wavenumber_spectrum, ECI_to_LVLH_matrices, find_LVLH_basis

"""
    wavenumber_spectrum(f_offset::Number, b⃗_bistatic::AbstractVector)

Compute the wavenumber spectrum for a given bistatic vector `b⃗_bistatic` at the offset frequency `f_offset` Hz about the centre frequency of 5.6 GHz.
"""
function wavenumber_spectrum(f_offset::Number, b⃗_bistatic::AbstractVector)
    2π / c₀ * (f₀ + f_offset) .* b⃗_bistatic
end

function ECI_to_LVLH_matrices(rs::AbstractVector, vs::AbstractVector)
    num_elements = length(rs)
    result_Qs = Array{SMatrix{3,3,Float64,9}}(undef, num_elements)
    for i = 1:num_elements
        (eᵣ, eₜ, eₙ) = find_LVLH_basis(rs[i], vs[i])
        result_Qs[i] = [transpose(eᵣ); transpose(eₜ); transpose(eₙ)]
    end
    result_Qs
end

"""
    find_LVLH_basis(r::AbstractVector, v::AbstractVector)

Find the local vertical local horizontal vector basis given the position vector r [m], and velocity vector v [m/s]. Returns a tuple (i⃗, j⃗, k⃗) of the three basis unit vectors of the LVLH frame. i⃗ points in the radial direction, k⃗ in the direction of angular momentum and j⃗ completes the right-handed set.
"""
function find_LVLH_basis(r::AbstractVector, v::AbstractVector)
    i⃗ = r ./ norm(r)
    h⃗ = r × v
    k⃗ = unit_v(h⃗)
    j⃗ = k⃗ × i⃗
    j⃗ = unit_v(j⃗)
    # k⃗ = -(r × v) ./ norm(r × v)
    # j⃗ = (i⃗ × k⃗) ./ norm(i⃗ × k⃗)
    # j⃗ = unit_v(v)
    # k⃗ = unit_v(j⃗ × i⃗)
    # i⃗ = unit_v(k⃗ × j⃗)
    (i⃗, j⃗, k⃗)
end

"""
    ECI_to_LVLH(rs, vs)

Constructs the direction-cosine matrix to transform from an Earth-centred frame to an LVLH frame. Rows of the matrix match the order of the returned basis vectors of find_LVLH_basis.
"""
function ECI_to_LVLH(rs, vs)
    (r, t, n) = find_LVLH_basis(rs, vs)
    [r'; t'; n']
end

"""
    EC_to_tangent_normal(rs, ws)

Constructs the direction-cosine matrix to transform from an Earth-centred frame to a tangent-normal frame. This function expects the same inputs as find_tanget_normal_basis. EC_to_tangent_normal is a convenience function that calls find_tanget_normal_basis, and arranges the returned basis vectors as rows in a 3 by 3 matrix in the following order: n, t, v.
"""
function EC_to_tangent_normal(rs, ws)
    (t, v, n) = find_tangent_normal_basis(rs, ws)
    [n'; t'; v']
end

"""
    find_tangent_normal_basis(rs, ws)

    Find the tanget-normal vector basis given the position vector r [m], and the wave number vector w. The tangent normal basis is defined by the tangent plane to the point r on the WGS-84 ellipsoid and the normal vector to it. The function returns the basis vectors t⃗, v⃗ and n⃗ that define the tangent normal frame. t⃗ is in the direction of w projected on the tangent plane at r, v⃗ is on the tangent plane perpendicular to t⃗ and n⃗ is the unit normal vector of the plane.
"""
function find_tangent_normal_basis(r::AbstractVector, w::AbstractVector)
    x, y, z = @views r[1], r[2], r[3]
    a_wgs84_sq = a_wgs84^2
    n⃗ = 2 .* [x / a_wgs84_sq, y / a_wgs84_sq, z / b_wgs84^2]
    n⃗ = n⃗ ./ norm(n⃗)
    w = w ./ norm(w)
    # project vector w on the tangent plane with normal n⃗
    t⃗ = w .- (w ⋅ n⃗) * n⃗
    t⃗ = t⃗ ./ norm(t⃗)
    # complete the right handed set
    v⃗ = n⃗ × t⃗
    (t⃗, v⃗, n⃗)
end
