################################################################################
#                                    Relative Orbit
################################################################################

export RelativeOrbit

"""
    RelativeOrbit{T}

This structure defines the relative orbit in terms of the Keplerian elements.

# Fields

* `Δa`: Semi-major axis [m].
* `Δe`: Eccentricity.
* `Δi`: Inclination [rad].
* `ΔΩ`: Right ascension of the ascending node [rad].
* `Δω`: Argument of perigee [rad].
* `Δf`: True anomaly [rad].

The epoch is not specified as it is assumed to be the same as that of
the KeplerianElements that the RelativeOrbit is defined relative to.

"""
struct RelativeOrbit{T<:Number}
    Δa::T
    Δe::T
    Δi::T
    ΔΩ::T
    Δω::T
    Δf::T
end

function RelativeOrbit(
    Δa::T1,
    Δe::T2,
    Δi::T3,
    ΔΩ::T4,
    Δω::T5,
    Δf::T6,
) where {T1<:Number,T2<:Number,T3<:Number,T4<:Number,T5<:Number,T6<:Number}
    T = promote_type(T1, T2, T3, T4, T5, T6)

    RelativeOrbit{T}(Δa, Δe, Δi, ΔΩ, Δω, Δf)
end

export RelativeOrbitalElements

"""
    RelativeOrbitalElements{T<:Real}

Store the parameters of a relative orbit between two formation-flying vehicles.

# Fields

* `δa`: Semi-major axis difference normalised by the semi-major axis of the chief vehicle [m].
* `δλ`: Relative mean longitude between between the vehicles [rad].
* `δe_x`: x component of the relative eccentricity vector [rad].
* `δe_y`: y component of the relative eccentricity vector [rad].
* `δi_x`: x component of the relative inclination vector [rad].
* `δi_y`: y component of the relative inclination vector [rad].

"""
struct RelativeOrbitalElements{T<:Real}
    δa::T
    δλ::T
    δe_x::T
    δe_y::T
    δi_x::T
    δi_y::T
end

function RelativeOrbitalElements(
    δa::T1,
    δλ::T2,
    δe_x::T3,
    δe_y::T4,
    δi_x::T5,
    δi_y::T6,
) where {T1<:Real,T2<:Real,T3<:Real,T4<:Real,T5<:Real,T6<:Real}
    T = promote_type(T1, T2, T3, T4, T5, T6)

    RelativeOrbitalElements{T}(δa, δλ, δe_x, δe_y, δi_x, δi_y)
end

################################################################################
#                                  Overloads
################################################################################

# + for KeplerianElements and RelativeOrbit
function Base.:+(x::SatelliteToolboxBase.KeplerianElements, y::RelativeOrbit)
    a = x.a + y.Δa
    e = x.e + y.Δe
    i = x.i + y.Δi
    Ω = x.Ω + y.ΔΩ
    ω = x.ω + y.Δω
    f = x.f + y.Δf
    SatelliteToolboxBase.KeplerianElements(x.t, a, e, i, Ω, ω, f)
end
