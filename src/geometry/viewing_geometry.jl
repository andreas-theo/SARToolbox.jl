function look_to_inc_angle(look_angle, h)
    if h >= 0
        asin((Rₑ + h) / Rₑ * sin(look_angle))
    else
        throw(DomainError(h, "The height must be nonnegative."))
    end
end

function look_angle_to_sr(look_angle, h)
    inc_angle = look_to_inc_angle(look_angle, h)
    β = inc_angle - look_angle
    h_tot = Rₑ + h
    √(Rₑ^2 + h_tot^2 - 2 * Rₑ * h_tot * cos(β))
end
