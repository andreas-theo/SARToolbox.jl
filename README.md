# SARToolbox

[![status-badge](https://ci.codeberg.org/api/badges/13246/status.svg)](https://ci.codeberg.org/repos/13246)

This package contains functions to model and simulate
synthetic-aperture radar (SAR) satellites.
