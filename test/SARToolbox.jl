using SARToolbox:
    unit_v, wave_vector, create_reference_orbit, from_rel_orbit, look_to_inc_angle
using SatelliteToolbox: Propagators

@testset "Unit vector calculation" begin
    r = [1, 1, 1]
    @test unit_v(r) == [1 / √3, 1 / √3, 1 / √3]
    r = [1, 0, 1]
    @test unit_v(r) == [1 / √2, 0, 1 / √2]
    r = [0, 0, 1]
    @test unit_v(r) == [0, 0, 1]
    r = [3, 0, 4]
    @test unit_v(r) == [3 / 5, 0, 4 / 5]
end

@testset "Wave vector calculation" begin
    θₗ = deg2rad(30)
    b̂ = [-√3 / 2, 0, -0.5]
    @test wave_vector(θₗ, 0) ≈ b̂ atol = eps(Float64)
    ψ = deg2rad(15)
    b̂ = [-√3 / 2 * (√6 + √2) / 4, (√6 - √2) / 4, -0.5 * (√6 + √2) / 4]
    @test wave_vector(θₗ, ψ) ≈ b̂ atol = eps(Float64)
end

@testset "Create a KeplerianElements from a relative orbit" begin
    orbp = create_reference_orbit("../assets/sentinel-1B")
    orbp_h = from_rel_orbit(Val(:J2), Propagators.mean_elements(orbp), 0, 0, 0)
    orb = Propagators.mean_elements(orbp)
    orb_h = Propagators.mean_elements(orbp_h)
    @test orb_h.t ≈ orb.t
    @test orb_h.a ≈ orb.a
    @test orb_h.e ≈ orb.e
    @test orb_h.i ≈ orb.i
    @test orb_h.Ω ≈ orb.Ω
    @test orb_h.ω ≈ orb.ω
    @test orb_h.f ≈ orb.f
end

@testset "Look to incident angle conversion" begin
    @test look_to_inc_angle(deg2rad(26), 693E3) ≈ deg2rad(29.0891) atol = 0.0005
    @test look_to_inc_angle(deg2rad(0), 693E3) == 0
    @test_throws DomainError look_to_inc_angle(0, -5)
end
