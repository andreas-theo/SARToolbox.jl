# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #
#
# Description
# ==========================================================================================
#
#   Tests related to the represenation of relative orbits
#
# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #

using SatelliteToolbox: KeplerianElements, date_to_jd

@testset "Construction of RelativeOrbit" begin
    Δa = 1
    Δe = 0.1
    Δi = 0.2
    ΔΩ = 0
    Δω = 0.3
    Δf = -0.1
    rel_orb = RelativeOrbit(Δa, Δe, Δi, ΔΩ, Δω, Δf)
    @test rel_orb isa RelativeOrbit{Float64}
    @test rel_orb.Δa == 1
    @test rel_orb.Δe ≈ 0.1 atol = eps(Float64)
    @test rel_orb.Δi ≈ 0.2 atol = eps(Float64)
    @test rel_orb.ΔΩ == 0
    @test rel_orb.Δω ≈ 0.3 atol = eps(Float64)
    @test rel_orb.Δf ≈ -0.1 atol = eps(Float64)

    orb = KeplerianElements(
        date_to_jd(1987, 1, 11, 15, 35, 0),
        693.982e3,
        0.0002111,
        93.5 |> deg2rad,
        150.000 |> deg2rad,
        50.000 |> deg2rad,
        15.66 |> deg2rad,
    )
    final_orb = orb + rel_orb
    @test final_orb === KeplerianElements(
        date_to_jd(1987, 1, 11, 15, 35, 0),
        693.982e3 + Δa,
        0.0002111 + Δe,
        (93.5 |> deg2rad) + Δi,
        (150.000 |> deg2rad) + ΔΩ,
        (50.000 |> deg2rad) + Δω,
        (15.66 |> deg2rad) + Δf,
    )
end

@testset "Construction of RelativeOrbitalElements" begin
    δa = 10
    δλ = 0.5
    δe_x = 0.2
    δe_y = 0
    δi_x = 0.3
    δi_y = -0.1
    rel_orb = RelativeOrbitalElements(δa, δλ, δe_x, δe_y, δi_x, δi_y)
    @test rel_orb isa RelativeOrbitalElements{Float64}
    @test rel_orb.δa == 10
    @test rel_orb.δλ ≈ 0.5 atol = eps(Float64)
    @test rel_orb.δe_x ≈ 0.2 atol = eps(Float64)
    @test rel_orb.δe_y == 0
    @test rel_orb.δi_x ≈ 0.3 atol = eps(Float64)
    @test rel_orb.δi_y ≈ -0.1 atol = eps(Float64)
end
