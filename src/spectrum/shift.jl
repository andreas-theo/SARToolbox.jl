export spectral_shift, spectral_shift_pre, fill_support!, spectral_shift_loop

"""
    spectral_shift_pre(s::AbstractArray, dim::Int, window_length::Int)

Pre-process array `s` in preparation for calculating the spectral shift over the given
dimension.

The array is indexed by a window centred on the middle index of the array, with
`window_length` samples along either side of the middle index.
"""
function spectral_shift_pre(s::AbstractArray, dim::Int, window_length::Int)
    # add check that window_length is < 2*size(s, dim)
    Rpre = CartesianIndices(size(s)[1:dim-1])
    Rpost = CartesianIndices(size(s)[dim+1:end])
    mid_index = ceil(Int, size(s, dim) / 2)
    # NOTE: window_length of 0 will produce a singleton dimension
    s_window = @views s[
        Rpre,
        mid_index-window_length:mid_index+window_length,
        Rpost,
    ]::SubArray{eltype(s),ndims(s)}
    s_xy = similar(s_window, SVector{2,Float64})
    fill_support!(s_xy, s_window)
end

"""
    fill_support!(out::AbstractArray, support::AbstractArray)

Fill array `out` with the contents of `support` in the along-track and cross-track
directions.

It is assumed that `support` is an array of 3-d arrays with the components of the spectral
support in the normal, along-track and cross-track directions and `out` has the same length
but each element is a 2-d array.
"""
function fill_support!(out::AbstractArray, support::AbstractArray)
    for i in eachindex(support)
        out[i] = support[i][2:3]
    end
    out
end

"""
    spectral_shift_loop(s1, s2, dim::Int, window_length::Int)

Index each entry of `s1` and `s2` for their 2nd and 3rd elements with a window of length
`window_length` each side of the middle index. Then compute the spectral shift along the
given dimension.
"""
function spectral_shift_loop(s1, s2, dim::Int, window_length::Int)
    s1_xy = map(r -> StaticArrays.SVector{2}(r[2:3]), s1)
    s2_xy = spectral_shift_pre(s2, dim, window_length)
    delta_s = spectral_shift(s1_xy, s2_xy, dim)::Array{Float64,ndims(s2)}
    min_indices = zeros(CartesianIndex{2}, size(delta_s, 3))
    for (i, sl) in enumerate(eachslice(delta_s, dims = 3))
        min_indices[i] = argmin(sl)
    end
    min_indices
end

"""
    spectral_shift(s_a, s_b, dim::Int)

Calculate the spectral shift between two spectra.

`s_a` is assumed to be the reference spectrum and the Euclidean distance between `s_a` and
`s_b` is calculated for all the entries of `s_b` over the given dimension. `s_a` is assumed
to have only one entry in the given dimension.
"""
function spectral_shift(s_a, s_b, dim::Int)
    out = similar(s_b, Float64)
    Rpre = CartesianIndices(size(s_b)[1:dim-1])
    Rpost = CartesianIndices(size(s_b)[dim+1:end])
    _spectral_shift!(out, s_a, s_b, Rpre, dim, Rpost) # separate function to avoid type-instability
end

"""
    _spectral_shift!(out, s_a, s_b, Rpre, dim::Int, Rpost)

Internal function to run the spectral shift along the specified dimension and write the
output to `out`.

Two cartesian iterators are used, `Rpre` and `Rpost`, that cover dimensions 1:`dim`-1 and
`dim`+1:`ndims(s_b)`.
"""
function _spectral_shift!(out, s_a, s_b, Rpre, dim::Int, Rpost)
    for Ipost in Rpost
        for i in axes(s_b, dim)
            for Ipre in Rpre
                out[Ipre, i, Ipost] = norm(s_a[Ipost[2]] - s_b[Ipre, i, Ipost])
            end
        end
    end
    out
end

"""
    sensitivity(s1, s2, r_p)

Calculate the height sensitivity from spectrum `s1` and `s2` along the direction of vector `r_p`.
"""
function sensitivity(s1, s2, r_p)
    Δspectrum = abs.(s1 .- s2)
    ê_r = r_p ./ norm(r_p)
    Δspectrum ⋅ ê_r
end
