function propagate_with_mean_elements!(
    orbp::OrbitPropagator{Tepoch,T},
    t::Number,
) where {Tepoch,T}
    r, v = Propagators.propagate!(orbp, t)
    orb = Propagators.mean_elements(orbp)
    orb, r, v
end

function propagate_with_mean_elements!(
    orbp::OrbitPropagator{Tepoch,T},
    t::AbstractVector,
) where {Tepoch,T}
    orv = propagate_with_mean_elements!.(orbp, t)
    orb = first.(orv)
    r = getindex.(orv, 2)
    v = last.(orv)
    orb, r, v
end
