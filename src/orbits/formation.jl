"""
    relative_orb_to_Δr(rel_orb::RelativeOrbitalElements, a::Real, u::Real)

Calculate the relative displacement vector of the chaser vehicle with respect to the main vehicle. The parameters of the relative orbit described using the rel_orb structure and the vector is calculated for an orbit with semi-major axis a [m] and at mean argument of latitude u [rad].

# Returns

A vector with 3 entires corresponding to the radial, tangential and normal components of the displacement.

"""
function relative_orb_to_Δr(rel_orb::RelativeOrbitalElements, a::Real, u::Real)
    δr_r = a * (rel_orb.δa - rel_orb.δe_x * cos(u) - rel_orb.δe_y * sin(u))
    δr_t =
        a * (
            -1.5 * rel_orb.δa * u +
            rel_orb.δλ +
            2 * (rel_orb.δe_x * sin(u) - rel_orb.δe_y * cos(u))
        )
    δr_n = a * (rel_orb.δi_x * sin(u) - rel_orb.δi_y * cos(u))
    SVector{3}(δr_r, δr_t, δr_n)
end

"""
relative_orb_to_Δv(rel_orb::RelativeOrbitalElements, a::Real, u::Real)

Calculate the relative velocity vector of the chaser vehicle with respect to the main vehicle. The parameters of the relative orbit described using the rel_orb structure and the vector is calculated for an orbit with semi-major axis a [m] and at mean argument of latitude u [rad].

# Returns

A vector with 3 entires corresponding to the radial, tangential and normal components of the displacement.

"""
function relative_orb_to_Δv(rel_orb::RelativeOrbitalElements, a::Real, u::Real)
    v = √(μₑ / a)
    δv_r = v * (-rel_orb.δe_y * cos(u) + rel_orb.δe_x * sin(u))
    δv_t = v * (-1.5 * rel_orb.δa + 2 * (rel_orb.δe_x * cos(u) + rel_orb.δe_y * cos(u)))
    δv_n = v * (rel_orb.δi_x * cos(u) + rel_orb.δi_y * sin(u))
    SVector{3}(δv_r, δv_t, δv_n)
end

"""
    chief_to_deputy_r(r_c::AbstractVector, v_c::AbstractVector, Δr::AbstractVector)

Compute the position vector of the deputy satellite from the state vector of the chief.

First convert to the LVLH frame, add the delta to position of the chief satellite and convert back to the inertial frame.

# Returns

A vector with 3 entires corresponding to the X, Y and Z component of the position.

"""
function chief_to_deputy_r(r_c::AbstractVector, v_c::AbstractVector, Δr::AbstractVector)
    (e_r, e_t, e_n) = find_LVLH_basis(r_c, v_c)
    Q_LVLH_to_EC = [e_r e_t e_n]
    r_c + Q_LVLH_to_EC * Δr
end

function chief_to_deputy_v(
    r_c::AbstractVector,
    v_c::AbstractVector,
    Δr::AbstractVector,
    Δv::AbstractVector,
    a,
)
    Ω = √(μₑ / a^3)
    Ω⃗ = [0, 0, Ω]
    (e_r, e_t, e_n) = find_LVLH_basis(r_c, v_c)
    Q_LVLH_to_EC = [e_r e_t e_n]
    v_c + Q_LVLH_to_EC * (Δv + Ω⃗ × Δr)
end
